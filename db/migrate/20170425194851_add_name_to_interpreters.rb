class AddNameToInterpreters < ActiveRecord::Migration[5.0]
  def change
    add_column :interpreters, :name, :string
  end
end
