class CreateCotations < ActiveRecord::Migration[5.0]
  def change
    create_table :cotations do |t|
      t.string :type
      t.integer :quantity

      t.timestamps
    end
  end
end
