class AddValueToCotation < ActiveRecord::Migration[5.0]
  def change
    add_column :cotations, :value, :double
  end
end
