Rails.application.routes.draw do

  root to: 'home#index'

  devise_for :interpreters
  namespace :interpreters do
    root to: 'dashboard#index'
  end

  devise_for :users
  namespace :users do
    root to: 'dashboard#index'
  end

  resources :cotations


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
