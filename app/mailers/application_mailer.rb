class ApplicationMailer < ActionMailer::Base
  default from: 'michael.vornes@outlook.com'
  layout 'mailer'
end
