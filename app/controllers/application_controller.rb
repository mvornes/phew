class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  layout :layout_by_resource

  def after_sign_in_path_for(resource)
    interpreters_root_path
  end

  def after_sign_out_path_for(resource)
    new_interpreter_session_path
  end

  protected
  def layout_by_resource
    return "layouts/session" if devise_controller?

    "layouts/application"
  end
end
