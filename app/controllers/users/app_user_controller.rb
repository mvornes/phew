class Users::AppUserController < ApplicationController
  layout 'users/layouts/application'

  before_action :authenticate_user!
end
