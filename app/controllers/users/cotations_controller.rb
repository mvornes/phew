class CotationsController < ApplicationController
  def index
    @cotations = Cotation.all
  end

  def show
    @cotation = Cotation.find(params[:id])
  end

  def new
  end

  def create
    @cotation = Cotation.new(cotation_params)
    @cotation.save
    redirect_to @cotation
  end

  private
  def cotation_params
    params.require(:cotation).permit(:type, :quantity)
  end

end
