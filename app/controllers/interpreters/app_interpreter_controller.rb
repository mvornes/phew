class Teachers::AppTeacherController < ApplicationController
  layout 'interpreters/layouts/application'

  before_action :authenticate_interpreter!
end
