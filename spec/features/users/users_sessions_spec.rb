require 'rails_helper'

describe "Users:Sessions", type: :feature do

  it "displays the user's perfil links after successful login" do
      user = FactoryGirl.create(:user, email: 'tcc@gp.utfpr.edu.br')

      visit new_user_session_path
      fill_in 'user_email', with: 'tcc@gp.utfpr.edu.br'
      fill_in 'user_password', with: 'password'

      find('input[name="commit"]').click # click_in 'Entrar'

      expect(page.current_path).to eq root_path
      expect(page).to have_selector('.btn', text: 'Meu perfil')
      expect(page).to have_selector('div.alert.alert-info',
                                    text: 'Login efetuado com sucesso!')
    end

end
