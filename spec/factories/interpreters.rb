FactoryGirl.define do
  factory :interpreter do
    name 'Alan Turing'
    sequence :email { |n| "interpreter#{n}@gmail.com" }
    password               "password"
    password_confirmation  "password"
  end
end
