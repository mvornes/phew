require 'rails_helper'

RSpec.describe Interpreters::DashboardController, type: :controller do

  describe "GET #index" do
    it "returns http 302 without user login" do
      get :index
      expect(response).to have_http_status(302)
    end

     it "returns http success" do
      user = FactoryGirl.create(:interpreter)
      sign_in(user, scope: :interpreter)

      get :index
      expect(response).to have_http_status(:success)
    end
  end

end
